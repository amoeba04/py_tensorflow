# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

if __name__ == '__main__':
    num = np.array([[1, 2, 3, 4, 5, 6, 7, 8, 9]])
    input1 = np.array([[2], [3], [4], [5], [6], [7], [8], [9]])
    input2 = np.array([[2], [3], [5], [8], [9]])
    x = tf.placeholder(tf.int32, [None, 1])
    y = tf.matmul(x, num)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        print(sess.run(y, feed_dict={x: input2}))
