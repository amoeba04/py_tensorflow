# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

if __name__ == "__main__":
    X = 8 * np.random.rand(100, 1) - 4
    Y = 2.7 - 1.75 * X + 0.5 * X * X + np.random.randn(100, 1)
    plt.plot(X, Y, 'ks')
    plt.title("y = 2.7 - 1.75*x + 0.5*x^2")
    plt.show()

    x = tf.placeholder(tf.float32)
    y_ = tf.placeholder(tf.float32)

    w1 = tf.Variable(tf.random_uniform([1]))
    w2 = tf.Variable(tf.random_uniform([1]))
    b = tf.Variable(tf.random_uniform([1]))
    y = w1 * x**2 + w2 * x + b

    dis = tf.reduce_mean(tf.square(y - y_))
    train = tf.train.GradientDescentOptimizer(0.001).minimize(dis)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for i in range(1000):
            sess.run(train, feed_dict={x: X, y_: Y})

        plt.plot(X, sess.run(w1)*X**2 + sess.run(w2)*X + sess.run(b), 'rs')
        plt.plot(X, Y, 'ks')
        plt.title("regression")
        plt.show()