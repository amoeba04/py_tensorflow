# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

if __name__ == "__main__":
    X = 2 * np.random.rand(100, 1)
    Y = 8 - 2.5 * X + np.random.randn(100, 1)
    plt.plot(X, Y, 'ks')
    plt.title("y = 8 - 2.5*x")
    plt.show()

    x = tf.placeholder(tf.float32)
    y_ = tf.placeholder(tf.float32)

    w = tf.Variable(tf.random_uniform([1]))
    b = tf.Variable(tf.random_uniform([1]))
    y = w * x + b

    dis = tf.reduce_mean(tf.square(y - y_))
    train = tf.train.GradientDescentOptimizer(0.1).minimize(dis)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for i in range(1000):
            sess.run(train, feed_dict={x: X, y_: Y})

        plt.plot(X, sess.run(w)*X + sess.run(b), 'r')
        plt.plot(X, Y, 'ks')
        plt.title("regression")
        plt.show()